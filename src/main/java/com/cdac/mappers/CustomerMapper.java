package com.cdac.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.cdac.pojo.Customer;

public class CustomerMapper implements RowMapper<Customer> {

	@Override
	public Customer mapRow(ResultSet rs, int rowNum) throws SQLException {
		Customer customer = new Customer();

		customer.setEmail(rs.getString("email"));
		customer.setPassword(rs.getString("password"));
		customer.setRole(rs.getString("role"));
		return customer;
	}

}
