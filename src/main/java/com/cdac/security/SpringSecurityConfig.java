package com.cdac.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserDetailsService userDetailsService;

	/*
	 * Configure Authentication Manager
	 */
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		/*
		 * By default spring comes with 2 Authentication providers :
		 * 
		 * 1. JDBC Authentication Provider
		 * 
		 * 2. In-Memory Authentication Provider
		 */

		/*
		 * JDBC Authentication Provider
		 */
		auth.authenticationProvider(getJDBCAuthenticationProvider());

	}

	/*
	 * JDBC Authentication Provider
	 */
	private AuthenticationProvider getJDBCAuthenticationProvider() {

		/*
		 * DaoAuthenticationProvider :
		 * 
		 * An AuthenticationProvider implementation that retrieves user details from a
		 * UserDetailsService.
		 */

		DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();

		/*
		 * Custom UserDetailsService
		 */
		authProvider.setUserDetailsService(userDetailsService);
		authProvider.setPasswordEncoder(getNoPasswordEncoder());

		return authProvider;
	}

	private PasswordEncoder getNoPasswordEncoder() {
		return NoOpPasswordEncoder.getInstance();
	}

}
