package com.cdac.pojo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.CredentialsContainer;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class Customer implements UserDetails, CredentialsContainer {

	private static final long serialVersionUID = 1L;
	/*
	 * Actual data fields related to customer
	 */
	private String email;
	private String password;
	private String role;

	/*
	 * Spring data fields related to authentication, we may use default values
	 */
//	private Boolean isAccountNonExpired;
//	private Boolean isAccountNonLocked;
//	private Boolean isCredentialsNonExpired;
//	private Boolean isEnabled;

	public Customer() {
	}

	public Customer(String email, String password, String role) {
		super();
		this.email = email;
		this.password = password;
		this.role = role;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	@Override
	public String toString() {
		return "Customer [email=" + email + ", password=" + password + ", role=" + role + "]";
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority(this.getRole()));
		return authorities;
	}

	@Override
	public String getUsername() {
		return this.email;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
//		return isAccountNonExpired();
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
//		return isAccountNonLocked();
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
//		return isCredentialsNonExpired();
	}

	@Override
	public boolean isEnabled() {
		return true;
//		return isEnabled();
	}

	@Override
	public void eraseCredentials() {
		this.password = "";
	}

}
